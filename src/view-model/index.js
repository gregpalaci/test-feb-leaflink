import { action, computed, observable } from 'mobx'

const endpoint =
  'https://rawgit.com/wvchallenges/se-exp-challenge-invoice/master/settings.json'

export default class ViewModel {
  @observable users = { customers: [] }
  @observable editingMode = false
  @observable editingId = null

  @computed get currentUser() {
    return this.users.customers.find(({ id }) => id === this.editingId) || {}
  }

  @computed get usersFormatted() {
    return this.users.customers.map(user => user)
  }

  @action.bound async fetchUsers() {
    this.users = await (await fetch(endpoint).catch(e => {}))
      .json()
      .catch(e => {})
  }
}

export const sharedState = new ViewModel()
