export default {
  srcDir: 'src',
  head: {
    title: 'The Test',
    // script: [
    //   {
    //     src: '/sw.js'
    //   }
    // ],
    meta: [
      { charset: 'utf-8' },
      {
        hid: 'description',
        name: 'description',
        content: 'a test'
      },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { 'http-equiv': 'x-ua-compatible', content: 'ie=edge,chrome=1' }
    ],
    htmlAttrs: {
      lang: 'en'
    }
  },
  css: ['~/assets/css/main.css'],
  modules: ['nuxt-purgecss', '@nuxtjs/pwa'],
  build: {
    postcss: {
      plugins: {
        tailwindcss: 'tailwind.config.js'
      }
    }
  },
  purgeCSS: {
    mode: 'postcss'
  },
  pwa: {
    manifest: {
      name: 'My Awesome App',
      short_name: 'app',
      lang: 'fa',

      display: 'standalone',
      scope: '/'
    }
  }
}
